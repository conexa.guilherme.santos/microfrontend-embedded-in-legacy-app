import { useEffect, useState } from 'react'
import listeningIcon from './assets/listening.gif'
import coinsIcon from './assets/coins.gif'
import successIcon from './assets/success.gif'

export default function App() {

  const [amount, setAmount] = useState('');
  const [paymentFinished, setPaymentFinished] = useState(false);

  // Apenas firula
  function changePaymentScreenAnimation(params) {
    setPaymentFinished(true)
    setTimeout(() => {
      setAmount(0)
      setPaymentFinished(false)
    }, 3000);
  }

  // Usado quando quiser mandar mensagens
  function finishPayment() {
    const id = Math.random()
    window.parent.postMessage({ id }, '*');
    // mais firula
    changePaymentScreenAnimation()
  }

  // listener inicial - isso deve estar em um lugar separado para ser reutilizado por outras partes da aplicação
  useEffect(() => {
    window.addEventListener('message', function(e) {
      const data = JSON.parse(e.data)
      console.log(data)
      setAmount(data.amount);
    });
  }, [])


  const PaymentForm = () => <>
      <h3 className="title">
        Payment
        <span className="title-icon">
          <img src={coinsIcon} alt=""/>
        </span>
      </h3>
      <p className="product-cost">Total: R$ {amount}</p>
      <input className="credit-card-number" placeholder="Número do cartão" />
      <input className="credit-card-owner" placeholder="Nome no cartão" />
      <input className="credit-cvv" placeholder="CVV" />
      <input className="credit-valid-date" placeholder="Data de validade" />
      <button onClick={finishPayment}>Concluir pagamento</button>
    </>

  const WaitingRoom = () => <>
        <div className="waiting-room">

          <img src={listeningIcon} alt="waiting"/>
          <p>esperando por um valor</p>
        </div>
      </>

  const PaymentFinishedScreen = () => <>
        <div className="waiting-room">
          <img src={successIcon} alt="success!"/>
          <p>Obrigado, seu pagamento foi recebido e o produto chegará a você em breve</p>
        </div>
      </>
  
  const currentScreen = () => {
    if (paymentFinished) return <PaymentFinishedScreen />
    else if (amount) return <PaymentForm />
    else return <WaitingRoom />
  }

  return (
    <div className="App">
      <div className="payment-card">
        <div className="new-app-tag">Adorável aplicativo em react XD</div>
        { currentScreen() }
      </div>
    </div>
  );
}
