# Como rodar

Comece com o app legado

- dentro da pasta legacy-app rode o comando `yarn` ou `npm install` para instalar as dependencias
- inicie o projeto com o comando `yarn start` ou `npm run start`

Agora o app de pagamento

- dentro da pasta payment-app rode o comando `yarn` ou `npm install` para instalar as dependencias
- inicie o projeto com o comando `yarn start` ou `npm run start`

O aplicativo react estará rodando na porta 3001 e o app legado está ouvindo esta porta.

A comunicação nesse exemplo se dá atraves do eventListener como no exemplo abaixo:

`
const message = {info: 'opa'}
window.addEventListener(message, () => alert('estou ouvindo))
`
e para enviar uma mensagem do iframe:

`window.parent.postMessage('message', '*');`

no caso do app legado usa-se o seguinte para enviar mensagens ao iframe:

`
const message = {info: 'opa'}
const iframe = document.getElementById('iframe-id')
iframe.contentWindow.postMessage(message, '*');
`

Fica aconselhado adicionar algum protocolo para o caso de não haver conflitos entre multiplos iframes, uma simples chave/valor no payload da mensagem informando de onde vem para onde vai já resolveria o problema.
